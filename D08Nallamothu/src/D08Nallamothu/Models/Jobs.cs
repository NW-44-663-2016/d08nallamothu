﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace DataVamshinath.Models
{
    public class Jobs
    {
        [ScallfoldColumn(false)]
        [Key]
        public int jobId { get; set; }
        [Required]
        public String JobName { get; set; }
        [Display(Name ="Location")]
        public int eventLocation { get; set; }
      



        public virtual Location Location { get; set; }
    }
}
