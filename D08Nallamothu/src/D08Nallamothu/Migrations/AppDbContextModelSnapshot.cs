using System;
using Microsoft.Data.Entity;
using Microsoft.Data.Entity.Infrastructure;
using Microsoft.Data.Entity.Metadata;
using Microsoft.Data.Entity.Migrations;
using DataVamshinath.Models;

namespace DataVamshinath.Migrations
{
    [DbContext(typeof(AppDbContext))]
    partial class AppDbContextModelSnapshot : ModelSnapshot
    {
        protected override void BuildModel(ModelBuilder modelBuilder)
        {
            modelBuilder
                .HasAnnotation("ProductVersion", "7.0.0-rc1-16348")
                .HasAnnotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn);

            modelBuilder.Entity("DataVamshinath.Models.Jobs", b =>
                {
                    b.Property<int>("jobId")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("JobName")
                        .IsRequired();

                    b.Property<int?>("LocationLocationID");

                    b.Property<int>("eventLocation");

                    b.HasKey("jobId");
                });

            modelBuilder.Entity("DataVamshinath.Models.Location", b =>
                {
                    b.Property<int>("LocationID")
                        .ValueGeneratedOnAdd();

                    b.Property<string>("Country");

                    b.Property<string>("County");

                    b.Property<double>("Latitude");

                    b.Property<double>("Longitude");

                    b.Property<string>("Place");

                    b.Property<string>("State");

                    b.Property<string>("StateAbbreviation");

                    b.Property<string>("ZipCode");

                    b.HasKey("LocationID");
                });

            modelBuilder.Entity("DataVamshinath.Models.Jobs", b =>
                {
                    b.HasOne("DataVamshinath.Models.Location")
                        .WithMany()
                        .HasForeignKey("LocationLocationID");
                });
        }
    }
}
