using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNet.Http;
using Microsoft.AspNet.Mvc;
using Microsoft.Data.Entity;
using DataVamshinath.Models;

namespace DataVamshinath.Controllers
{
    [Produces("application/json")]
    [Route("api/Jobs")]
    public class JobsController : Controller
    {
        private AppDbContext _context;

        public JobsController(AppDbContext context)
        {
            _context = context;
        }

        // GET: api/Jobs
        [HttpGet]
        public IEnumerable<Jobs> GetJobs()
        {
            return _context.Jobs;
        }

        // GET: api/Jobs/5
        [HttpGet("{id}", Name = "GetJobs")]
        public IActionResult GetJobs([FromRoute] int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Jobs jobs = _context.Jobs.Single(m => m.jobId == id);

            if (jobs == null)
            {
                return HttpNotFound();
            }

            return Ok(jobs);
        }

        // PUT: api/Jobs/5
        [HttpPut("{id}")]
        public IActionResult PutJobs(int id, [FromBody] Jobs jobs)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            if (id != jobs.jobId)
            {
                return HttpBadRequest();
            }

            _context.Entry(jobs).State = EntityState.Modified;

            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JobsExists(id))
                {
                    return HttpNotFound();
                }
                else
                {
                    throw;
                }
            }

            return new HttpStatusCodeResult(StatusCodes.Status204NoContent);
        }

        // POST: api/Jobs
        [HttpPost]
        public IActionResult PostJobs([FromBody] Jobs jobs)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            _context.Jobs.Add(jobs);
            try
            {
                _context.SaveChanges();
            }
            catch (DbUpdateException)
            {
                if (JobsExists(jobs.jobId))
                {
                    return new HttpStatusCodeResult(StatusCodes.Status409Conflict);
                }
                else
                {
                    throw;
                }
            }

            return CreatedAtRoute("GetJobs", new { id = jobs.jobId }, jobs);
        }

        // DELETE: api/Jobs/5
        [HttpDelete("{id}")]
        public IActionResult DeleteJobs(int id)
        {
            if (!ModelState.IsValid)
            {
                return HttpBadRequest(ModelState);
            }

            Jobs jobs = _context.Jobs.Single(m => m.jobId == id);
            if (jobs == null)
            {
                return HttpNotFound();
            }

            _context.Jobs.Remove(jobs);
            _context.SaveChanges();

            return Ok(jobs);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool JobsExists(int id)
        {
            return _context.Jobs.Count(e => e.jobId == id) > 0;
        }
    }
}